import tempfile

import pytest


def test_run(hub):
    with tempfile.TemporaryDirectory() as tempdir:
        hub.pop_create.init.run(directory=tempdir, subparsers=[])


def test_run_bad_subparser(hub):
    with tempfile.TemporaryDirectory() as tempdir:
        with pytest.raises(AttributeError):
            hub.pop_create.init.run(
                directory=tempdir, subparsers=["nonexistant subparser"]
            )
